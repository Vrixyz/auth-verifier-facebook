#[macro_use]
extern crate serde_derive;
extern crate serde_json;
extern crate reqwest;


const ACCESS_TOKEN_ENDPOINT: &str = "https://graph.facebook.com/oauth/access_token";
const TOKEN_INSPECTION_ENDPOINT: &str = "https://graph.facebook.com/debug_token";

#[derive(Debug, Deserialize)]
struct FacebookAccessToken {
    access_token: String,
    token_type: String,
}

fn get_app_access_token(app_id: &str, app_secret: &str) -> reqwest::Result<FacebookAccessToken> {
    let request = format!(
        "{}?client_id={}&client_secret={}&grant_type=client_credentials",
        ACCESS_TOKEN_ENDPOINT, app_id, app_secret
    );
    println!("request: {}", request);
    let test = reqwest::get(&request)?.json();
    println!("result: {:#?}", test);
    test
}

#[derive(Debug, Deserialize)]
pub struct Data {
    app_id: String,
    #[serde(rename = "type")]
    data_type: String,
    application: String,
    expires_at: i64,
    pub is_valid: bool,
    scopes: Vec<String>,
    pub user_id: String,
}

#[derive(Debug, Deserialize)]
pub struct TokenVerification {
    pub data: Data,
}

pub fn verify_token(
    app_id: &str,
    app_secret: &str,
    token_to_verify: &str,
) -> reqwest::Result<TokenVerification> {
    let app_access_token = get_app_access_token(app_id, app_secret)?;

    let request = format!(
        "{}?input_token={}&access_token={}",
        TOKEN_INSPECTION_ENDPOINT, token_to_verify, app_access_token.access_token
    );
    println!("request: {:#?}", request);
    reqwest::get(&request)?.json()
}
